import { Component, OnInit } from '@angular/core';
import { Product } from '../producrt.model';

@Component({
  selector: 'app-shoppingcart',
  templateUrl: './shoppingcart.component.html',
  styleUrls: ['./shoppingcart.component.css'],
})
export class ShoppingcartComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
  products: Product[] = [
    {
      id: '1',
      name: 'Samsung Galaxy A20',
      description: 'Màn hình vô cực, camera kép góc siêu rộng',
      thumbnail:
        'https://img.global.news.samsung.com/vn/wp-content/uploads/2019/03/Galaxy-A50-Mat-truoc-3.jpg',
      price: 108.5,
      quantity: 2,
    },
    {
      id: '2',
      name: 'Samsung Galaxy X',
      description:
        'Đây sẽ là mẫu smartphone đầu tiên có thể tận dụng linh hoạt ưu điểm vượt trội của màn hình OLED có thể gập được.',
      thumbnail:
        'https://cdn.tgdd.vn/Files/2018/05/10/1087515/ro-ri-hinh-anh-chiec-dien-thoai-gap-man-hinh-doc-dao-cua-samsung-3.jpg',
      price: 130.99,
      quantity: 1,
    },
  ];

  numberItems: number = 3;
  subTotal: number = 347.99;

  removeProduct(productID: string) {
    const index = this.products.findIndex(
      (product) => product.id === productID
    );
    if (index !== -1) {
      this.products.splice(index, 1);
    }
    this.tinhTong();
  }
  //Update Quantity
  updateQuantity(index: number) {
    let nbItems = 0;

    for (const product of this.products) {
      nbItems += index;
    }

    this.numberItems = nbItems;
  }
  //Tính tổng
  tinhTong() {
    let numberItem = 0;
    let subTotal = 0;

    for (const product of this.products) {
      numberItem += product.quantity;
      subTotal += product.price * product.quantity;
    }
    this.numberItems = numberItem;
    this.subTotal = subTotal;
  }
}
