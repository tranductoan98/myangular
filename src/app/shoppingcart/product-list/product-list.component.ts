import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css'],
})
export class ProductListComponent implements OnInit {
  @Input()
  products!: any[];

  @Output() onRemoveProduct = new EventEmitter
  @Output() onupdateQuantity = new EventEmitter

  constructor() {}

  ngOnInit(): void {}

  removeProduct(productID: string): void {
    this.onRemoveProduct.emit(productID);
  }
  updateQuantity(element: any){
    this.onupdateQuantity.emit(element.value);
  }

}
