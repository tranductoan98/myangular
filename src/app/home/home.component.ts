import { Component, Input, OnInit } from '@angular/core';
import { Product } from '../producrt.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  constructor() { }

  ngOnInit(): void {
  }
  products: Product[] = [
    {
      id: '1',
      name: 'Samsung Galaxy A20',
      description: 'Màn hình vô cực, camera kép góc siêu rộng',
      thumbnail:
        'https://img.global.news.samsung.com/vn/wp-content/uploads/2019/03/Galaxy-A50-Mat-truoc-3.jpg',
      price: 108.5,
      quantity: 2,
    },
    {
      id: '2',
      name: 'Samsung Galaxy X',
      description:
        'Đây sẽ là mẫu smartphone đầu tiên có thể tận dụng linh hoạt ưu điểm vượt trội của màn hình OLED có thể gập được.',
      thumbnail:
        'https://cdn.tgdd.vn/Files/2018/05/10/1087515/ro-ri-hinh-anh-chiec-dien-thoai-gap-man-hinh-doc-dao-cua-samsung-3.jpg',
      price: 130.99,
      quantity: 1,
    },
  ];
  prods!: Product[];
  addcart(product: Product){
    this.prods?.push(product);
    console.log(product.id);

    this.prods.forEach((item, product) => {
      console.log("productID:"+item.id);
    });
  }
}
