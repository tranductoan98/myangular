import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule} from './app-routing.module';
import { AppComponent } from './app.component';

import { CartHeaderComponent } from './shoppingcart/cart-header/cart-header.component';
import { CartSummaryComponent } from './shoppingcart/cart-summary/cart-summary.component';
import { ProductListComponent } from './shoppingcart/product-list/product-list.component';
import { PromoCodeComponent } from './shoppingcart/promo-code/promo-code.component';
import { ShoppingcartComponent } from './shoppingcart/shoppingcart.component';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    CartHeaderComponent,
    CartSummaryComponent,
    ProductListComponent,
    PromoCodeComponent,
    ShoppingcartComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
